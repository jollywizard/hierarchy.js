
const src = '../src'

const resolve = require(`${src}/resolve`)
const assert = require('assert')

class A {}
const a = new A()

class B extends A{}
const b = new B()

class O extends Object{}

describe('Module: `resolve`', function() {

  it('offers camel case and point delimited namespaces.', ()=>{
    assert.equal(resolve.ClassFor, resolve.class.for)
    assert.equal(resolve.ClassOf, resolve.class.of)
    assert.equal(resolve.PrototypeFor, resolve.prototype.for)
    assert.equal(resolve.PrototypeOf, resolve.prototype.of)
    assert.equal(resolve.Typename, resolve.typename)
    assert.equal(resolve.KeysOf, resolve.keys.of)
    assert.equal(resolve.NamesOf, resolve.names.of)
    assert.equal(resolve.DescriptorsOf, resolve.DescriptorsOf)
  })

  describe('Class awareness', function() {
    it('knows a class extends Function', ()=>{
      assert.equal(resolve.class.of(A), Function)
    })
    it('can resolve an object to its class', ()=> {
      assert.equal(resolve.class.of(a), A)
    })
    it('can normalize results so instance matches class.', ()=>{
      assert.equal(resolve.class.of(a), resolve.class.for(a))
      assert.equal(resolve.class.for(A), resolve.class.for(a))
    })
  })

  describe('Prototype awareness', ()=>{
    it('knows the prototype of a class is its supertype function.', ()=>{
      assert.equal(resolve.prototype.of(A), Function.prototype)
      assert.equal(resolve.prototype.of(B), A)
      assert.equal(resolve.prototype.of(O), Object)
    })

    it('can get the prototype declared for the class instead', function() {
      assert.equal(resolve.prototype.for(A), A.prototype)
      assert.equal(resolve.prototype.for(B), B.prototype)
    })

    it('can get the prototype of an instance using either method.', function() {
      assert.equal(A.prototype, resolve.prototype.of(a))
      assert.equal(A.prototype, resolve.prototype.for(a))
    })
  })

  describe('Typename awareness', ()=>{
    it('resolves an instance, class, or prototype to the class name string.', ()=> {
        let type = A.name
        assert.equal(resolve.typename(A), type)
        assert.equal(resolve.typename(a), type)
        assert.equal(resolve.typename(A.prototype), type)
    })
  })
})

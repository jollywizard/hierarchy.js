const src = './src'

const chain = require(`${src}/chain`)
const resolve = require(`${src}/resolve`)

module.exports.chain = chain
module.exports.resolve = resolve
